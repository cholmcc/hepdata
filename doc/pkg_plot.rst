=================================
Plotting package - `hepdata.plot`
=================================

.. toctree::
   :maxdepth: 2

Here are some simple code to plot HEPData submission.   For example

.. plot::
   :include-source: True

   import sys
   sys.path.append('../examples')
   sys.path.append('..')

   from simple import make
   from fill import fill
   from plot import plot
   import hepdata.io as hi

   s,m,t = make()
   fill(t)
   plot(t)

.. automodapi:: hepdata.plot
	   
.. EOF
   
