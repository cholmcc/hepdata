==========
Unit tests
==========

.. toctree::
   :maxdepth: 2


.. _test_hepdata:

Main package ``hepdata``
========================

test_accepts
------------

We run some tests against the accepts decorator.  Note, accepts may
end up _not_ parsing a value for a non-default positional argument,
which makes it raise an exception.

.. literalinclude:: ../test/test_accepts.py


test_singleton
--------------

Test of the Singleton *metaclass*. We define a tester dummy class. 

.. literalinclude:: ../test/test_singleton.py


test_rounder
------------

Test of the Rounder framework.

.. literalinclude:: ../test/test_rounder.py



test_column
-----------

As Column is really a base class, we define a dummy data type and
column type to test the various features of the Column class.

.. literalinclude:: ../test/test_column.py


test_value
----------

Test of the single dependent value Value class.  Here we do quite a
bit, since this is in many ways the work-horse of the whole package.

.. literalinclude:: ../test/test_value.py


test_dependent
--------------

Test of Dependent column type.  Mainly selection based on column name
and qualifiers.

.. literalinclude:: ../test/test_dependent.py


test_bin
--------

Test of the single independent value class. We test access to the
members. The keywords ``bounds`` and ``alwaystuple`` for
:meth:`Bin.xe` are illustratd. Rounding and such is dealt with
in test_rounder.

.. literalinclude:: ../test/test_bin.py


test_independent
----------------

Test of Independent column type.  Mainly selection based on column name.

.. literalinclude:: ../test/test_independent.py


test_licensed
-------------

Test of the Licensed base class. We define a dummy derivative. 

.. literalinclude:: ../test/test_licensed.py


test_resource
-------------

Test of the resource auxillary class

.. literalinclude:: ../test/test_resource.py


		    
test_meta
---------

Test of the Meta class for storing Table meta data.

.. literalinclude:: ../test/test_meta.py


test_table
----------

Test of the data table Table class. 

.. literalinclude:: ../test/test_table.py


test_search
-----------

Test of searching `inSpire <http://insirehep.net>`. Note, this test is
normally disabled because it can be slow.

.. literalinclude:: ../test/test_search.py


test_submission
---------------

Test of the Submission class. 

.. literalinclude:: ../test/test_submission.py
		    

.. _test_hepdata.combiner:
   
Combination package ``hepdata.combiner``
========================================

test_combiner
-------------

Tests of the Combiner framework.  This include the Averager and Adder
(and Adder2) classes.

.. literalinclude:: ../test/test_combiner.py


.. _test_hepdata.io:

Input/Output package ``hepdata.io``
===================================

test_io
-------

Test of YAML I/O and validation.  Note, the validator class defines a
compatibility mode that allows ``associated_records`` on Meta and
Submission, as well as the ``name`` property of ``Table`` (for single
file submissions).

.. literalinclude:: ../test/test_io.py

		    
.. _test_hepdata.names:
   
Names package ``hepdata.names``
===============================

test_phrase
-----------

Test of phrases database

.. literalinclude:: ../test/test_phrase.py


test_particle
-------------

Test of particles database

.. literalinclude:: ../test/test_particle.py


test_unit
---------

Test of units database

.. literalinclude:: ../test/test_unit.py

test_system
-----------

Test of systems database

.. literalinclude:: ../test/test_system.py

test_variable
-------------

Test of variables database

.. literalinclude:: ../test/test_variable.py
   

test_observable
---------------

Test of observables database

.. literalinclude:: ../test/test_observable.py
   

test_helper
-----------

Test of helper class using names databases 

.. literalinclude:: ../test/test_helper.py
   

.. EOF		
