==================================
Walk-through of the API with names
==================================

The names API (in :mod:`hepdata.names`) provides a simple, yet
powerful, way of defining submission that adhere to the naming
conventions used by HEPData_.  The idea is pretty simple:

    We make symbols that we can use in the code to look up various
    properties associated with these symbols.  We group these into
    categories (classes) to allow mnemonic use

The classes are 

- :class:`hepdata.names.Phrase` 
  Phrases to use in the submissions file for each table 
- :class:`hepdata.names.Particle` 
  Particle types 
- :class:`hepdata.names.System` 
  Collision systems 
- :class:`hepdata.names.Unit` 
  Units 
- :class:`hepdata.names.Variable`
  Variables (e.g., PT) with optional units 
- :class:`hepdata.names.Observable`
  Observables (e.g. D2N/DPT/DYRAP) with optional units 

Each of these classes have constants that we can use to ensure
referential integrity throughout.   The constants are created
dynamically at load time and can be expanded (or even changed) as
needed.

Furthermore, the ``names`` package also supply methods to pretty
format all of the above, which is really use full when plotting, for
example.

The databases can easily be expanded by providing additional YAML
files that can be loaded by :func:`hepdata.names.load`.  Ideally,
HEPData_ would provide these databases, possibly by extracting known
variables, observables, phrases, etc. from their internal databases.

And finally, the package provides an easier way of defining
submission, tables, and so on.   This is what we'll see here.  The
main class for that purpose is :class:`hepdata.names.Helper` and
wrapper functions. 

Create a submission
===================

To start with we need to create a :class:`hepdata.Submission` object.
This is pretty simple via the no-argument constructor or
:class:`hepdata.Submission` or the alias hepdata.submission.

>>> import hepdata as hd
>>> s = hd.Submission()

As described elsewhere (:ref:`walkthrough`), we can add various pieces
of information to the submission, such as

- `Record IDs` : The are identifiers associated with the submission.
  These can be

  - `inSpire`_ record IDs,
  - DOI of the publication,
  - `CDS`_ record IDs,
  - `arXiv`_ identifier,
  - or some other identifier

- `Comment` : A free-form description of the submission

- `Resources` : References to external information, or additional
  files to attach to the submission.

Also remember we can do

>>> s.fill('journal reference')

to search inSpire and fill in as many values as possible.

Tables of data
==============

The :class:`hepdata.names.Helper` class provides some useful features
to quickly set up a table.  Let's do the same thing we did in another
example (:ref:`complete`), but using the :class:`hepdata.names.Helper`
functionality.

We start by defining some constants, just to make it easier to read

>>> import hepdata.names as hn
>>> cents      = [(0,10),(10,20),(20,40)]
>>> sys        = hn.System.AUAU
>>> sqrts      = 200
>>> var        = [hn.Variable.YRAP, hn.Variable.ETARAP]
>>> obs        = [hn.Observable.Y_DENSITY, hn.Observable.ETA_DENSITY]
>>> parts      = [hn.Particle.PI_P,hn.Particle.PI_M,
...               hn.Particle.K_P, hn.Particle.K_M,
...               hn.Particle.P,   hn.Particle.PBAR]

Note how we used constants (rather than strings) to set up various
pieces of information.   Next, we'd like to add some qualifiers, but
to make it easy, we do this via some lists so that each column will
get its own set of qualifiers

>>> tmp        = [[dict(name=hn.Variable.CENTRALITY,
...                     value='{}-{}'.format(c[0],c[1])),
...                dict(name='sys:Centrality',
...                     value=(c[1]-c[0])/2, units=hn.Unit.PCT),
...                dict(name='sys:Bla',
...                     value=(c[1]-c[0])/2, units=hn.Unit.PCT)]
...               for c in cents]
>>> quals      = []
>>>  for t in tmp:
...     quals.append(t)
...     quals.append(t)

The reason we duplicate in the end is because we have two dependent
variables per centrality.

With this on hand we can define all our independent and dependent
variables easily

>>> t, m, i, d = hn.columns(s,None,sys,sqrts,var,obs*3,parts,
...                         name=name,filename=name+'_tab.yaml',
...                         loc='name',more=quals)


This will set up our meta information and all our independent and
dependent variable columns.  It will also set various pieces of meta
data, such as ``reactions``, ``cmenergies``, ``observables``, and
``phrases`` based on the passed identifiers.

The point is, that each Variable and Observable has a set of
associated phrases, so that using these with
:func:`hepdata.names.columns` will automatically set those phrases on
the meta data.


Fill in values
--------------

Filling in the values is no different than before.  

Finishing off the submission
============================


Visualising the data
--------------------

We can use the fact that the :mod:`hepdata.names` package knows how
to pretty format most stuff to make our visualisation more pleasing.
All we need to do is to make an instance of the
hepdata.names.Beautifier class and pass it on to our
:class:`hepdata.plot.Plotter` instance.

.. plot::
   :include-source: True

   import sys
   sys.path.append('../examples')
   sys.path.append('..')

   from names import make
   from fill import fill
   from plot import plot
   import hepdata.names as hn

   s,m,t = make()
   fill(t)
   plot(t,hn.Beautifier())

.. _inSpire: http://inspirehep.net
.. _HEPData: http//hepdata.net
.. _CDS: https://cds.cern.ch
.. _arXiv: http://arxiv.org
.. _NumPy: http://numpy.org

.. EOF		
