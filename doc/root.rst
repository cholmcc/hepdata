====================
Read in ROOT objects
====================

The module :mod:`hepdata.root` defines the utility class
:class:`hepdata.root.ToHEPData` that allows for easy conversion of
common ROOT classes to the HEPData format.  Supported ROOT objects are
of the classes

- :class:`TH1`
- :class:`TGraphErrors`
- :class:`TGraphAsymmErrors`
- :class:`TMultiGraph`

The last one - :class:`TMultiGraph` - allows us to define multiple
uncertainties.  The assumption is that the first graph in the
multi-graph sets the data points and uncertainty, while the remaining
graphs defines additional uncertainties.

Example
=======

Create the ROOT data
--------------------

First, we define some functions to make our ROOT data

.. literalinclude:: ../examples/fromroot.py
   :lines: 12-77

This will make a histogram, two graphs, and a multi-graph and write
those objects to disk

Convert the ROOT data
---------------------

We import our code

.. literalinclude:: ../examples/fromroot.py
   :lines: 78-81

First, we make a function to plot the converted data - just to see

.. literalinclude:: ../examples/fromroot.py
   :lines: 82-100

Next, we open the file created by the above, retrieve the histogram,
two graphs, and multi-graph, and define our submission object.

After that, we loop over the retrieved objects and make a table for
each of them, and in the end we plot the data. 

.. literalinclude:: ../examples/fromroot.py
   :lines: 101-124

All together now
----------------

Let's execute the function to make the ROOT data file and then the
function to convert the objects to HEPData 

.. literalinclude:: ../examples/fromroot.py
   :lines: 125-
