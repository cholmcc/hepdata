=====================================
Combiner package - `hepdata.combiner`
=====================================

.. toctree::
   :maxdepth: 2

The usage is really simple

- Declare a combiner (either LinearSigma or LinearVariance)
- Add observations to the combiner 
- Calculate the result 

Expectations from calculator at

    `<http://www.slac.stanford.edu/~barlow/java/statistics5.html>`_

(Chrome and Firefox do not suppor Java applets anymore, so use the
appletviewer)

Applet byte code available at 

    `<http://www.slac.stanford.edu/~barlow/java/asymmetricstatistical.class>`_

The code is available as 

- `<http://www.slac.stanford.edu/~barlow/java/asymmetric.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/asymmetricstatistical.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/asyslimit.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/CombineErrors.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/CombineResults.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/pair.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/Ruler.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/syslim1.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/syslimit.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/syslimnew.java>`_
- `<http://www.slac.stanford.edu/~barlow/java/syslimold.java>`_


Test cases
----------

See :ref:`test_hepdata.combiner`

.. automodapi:: hepdata.combiner
	   
.. EOF
   
