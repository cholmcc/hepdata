==========================
I/O package - `hepdata.io`
==========================

.. toctree::
   :maxdepth: 2

The classes in this package allows for export/import of YAML formatted
data files and submissions - from file system files or from Zip or Tar
(including compressed) archives.

Validation of the data can be done on the fly on both input and
output. 


Test cases
----------

See :ref:`test_hepdata.io`

.. automodapi:: hepdata.io
	   
.. EOF
   
