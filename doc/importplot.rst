================================================
Import and plot all tables from HEPData download
================================================


The example script ``example/hepdata_plot.py`` will import a full
submission and plot all tables from the submission, masking a best
effort to show the result as best possible.  It is not omnipotent, but
can serve as inspiration for other scripts.  The code follows

.. literalinclude:: ../examples/hepdata_plot.py

Let's try to use this on the submission
`https://doi.org/10.17182/hepdata.76987 <https://www.hepdata.net/record/ins1512299>`_

.. plot::
   :include-source: True

   import sys
   sys.path.append('..')

   from examples.hepdata_plot import *

   a = All('../examples/data/1512299.zip')
   a.plot(beauty=False) 
   

   

