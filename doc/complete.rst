.. _complete:

==================
A complete example
==================

Initialise the submission
=========================

In the code below, we make a submission with a single table in it.

The collision system is set Au-Au at
:math:`\sqrt{s_{\mathrm{NN}}}=200\mathrm{Ge\!V}`
      
The table contains the independent variables

- Pseudorapidity :math:`\eta`, named ``ETARAP``
- Rapidity :math:`y`, named ``YRAP``

The table contains 2 dependent variables for each of the centrality
ranges

.. math::
   (0-10)\%, (10-20)\%,\ \mathrm{and}\ (20-40)\%

The set the qualifier ``CENTRALITY`` with the units ``PCT`` and values
``0-10``, ``10-20``, and ``20-40`` for each set, respectively.

The 2 dependent variables for each centrality bin are 

- The
  :math:`\mathrm{\pi}^{\pm}+\mathrm{K}^{\pm}+\mathrm{p}+\overline{\mathrm{p}}`
  pseudorapidity density
  :math:`\mathrm{d}N/\mathrm{d}\eta`

- The
  :math:`\mathrm{\pi}^{\pm}+\mathrm{K}^{\pm}+\mathrm{p}+\overline{\mathrm{p}}`
  :math:`\mathrm{d}N/\mathrm{d}y`

We also declare two `common` systematic uncertainties labelled
``sys:Centrality`` and ``sys:Bla`` both of which are relative (they
have units ``PCT``) 
	
.. literalinclude:: ../examples/simple.py
   :lines: 5-
     
The function ``make`` returns the submission, the meta information,
and the table

Fill in values
==============

The code below will fill in pseudo-random data into our table created
by the function ``make`` above.

The function ``nm`` just returns the normal distribution evaluated at
:math:`y`, with scale :math:`a`, and width :math:`\sigma`

.. math::
   a \frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{1}{2}\frac{y^2}{\sigma^2}}

.. literalinclude:: ../examples/fill.py
   :lines: 5-10

The function ``fy`` evaluates ``nm`` for :math:`\sigma=1`.

.. literalinclude:: ../examples/fill.py
   :lines: 11-13

``y2eta`` calculates the pseudorapidity :math:`\eta` corresponding to
the rapidity :math:`y` as

.. math::

   \eta = \frac{1}{2}\log\left[
       -\frac{(1+2a^2)t^2 - 2t\sqrt{a^2+1}\sqrt{a^2+t^2+1}+1}
             {t^2-1}\right]

where :math:`t = \tanh{y}`, and :math:`a=\frac{p_{\mathrm{T}}}{m}=2.5`
is the effective ratio of transverse momentum to mass.

.. literalinclude:: ../examples/fill.py
   :lines: 14-19

The function ``feta`` thus evaluates

.. math::
   \frac{\mathrm{d}N}{\mathrm{d}\eta} = 
   \frac{\partial y}{\partial \eta}\frac{\mathrm{d}N}{\mathrm{d}y}

where

.. math::

   \frac{\partial y}{\partial \eta} = \frac1{\beta}
   = \frac{1}{\sqrt{1 + a^2 / \cosh{\eta}}}

.. literalinclude:: ../examples/fill.py
   :lines: 20-22

The functions ``se`` and ``ae`` calculates symmetric and asymmetric
uncertainties, respectively, relative to the argument ``v`` by the
factor ``f`` and with some Gaussian smearing of :math:`v\frac{f}{2}`.

.. literalinclude:: ../examples/fill.py
   :lines: 23-28

Note, the ``fill`` function below uses the selection mechanism to
access the independent and dependent variables.  The function does not
return anything, but modifies the passed in Table object.

.. literalinclude:: ../examples/fill.py
   :lines: 29-

Plotting the data
=================

For visual validation, we would like to plot the data.  The function
``plot`` below does this pretty straight forwardly.

First, we make 2 subplots in a single figure, and select the
independent and dependent columns we want to draw.

Next, we set up the uncertainty propagation keywords.  Here, we want
to stack the uncertainties by adding them using the linear variance
model (see hepdata.combiner.LinarVariance).

Next, we loop over all the columns of the light hadron rapidity
density, and plot these on the first subplot.

Next, we do the same for the light hadron pseudorapidity density.
Note, here we want to give different plot parameters for each
uncertainty so we use the service function
:meth:`hepdata.plot.Plotter.emptyParams` and
:meth:`hepdata.plot.Plotter.emptyCommonParams`

Finally, we add legends to each of the subplots.

.. literalinclude:: ../examples/plot.py
   :lines: 6-

Run the whole thing
===================

Let's see the plot, and export to a zip file

.. plot::
   :include-source: True

   import sys
   sys.path.append('../examples')
   sys.path.append('..')

   from simple import make
   from fill import fill
   from plot import plot
   import hepdata.io as hi

   s,m,t = make()
   fill(t)
   plot(t)
   
   hi.dump(s,'submission.zip')

   





.. EOF
   
