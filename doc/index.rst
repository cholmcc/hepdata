======================================
Welcome to (py)HEPData's documentation
======================================

This Python (3+) package aims to make it easier to generate submission
to HEPData_

.. toctree::
   :maxdepth: 2

   examples 	      
   api
   test


This library provides tools for creating, exporting, and importing
HEPData_ submissions.  The :ref:`api` is relatively simple and
decidedly object oriented.  However, the library provides many useful
features when working with HEPData_ submissions:

- Flexible specification of :ref:`create_submission` and :ref:`create_meta`. 
- Flexible specification of table columns split between
  :ref:`create_indep`  and :ref:`create_dep`.
- Flexible methods to :ref:`fill_in`
- A suggestion of how to specify :ref:`common_uncer`
- :ref:`round_vals`
- Directly :ref:`plot_data` with selection of columns, legends, etc.
- :ref:`export_sub` to YAML format, including as a Zip or Tar
  (possibly compressed) archive directly
- :ref:`import_data` directly from HEPData downloads 
- Direct validation of data, both on input and output (see
  :class:`hepdata.io.Validator`) 
- Flexible propagation of uncertainties, including asymmetric
  uncertainties, stacking of uncertainties, and so on (see
  :class:`hepdata.Propagator` and :mod:`hepdata.combiner`)
- Names database to ensure use of names recognised by HEPData_ (see
  :mod:`hepdata.names`) 
- Easy table creation with automatic phrase specification (see
  :class:`hepdata.names.Helper` and functions in :mod:`hepdata.names`).

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _HEPData : <https://hepdata.net>
.. _inSpire : <https://inspirehep.net>
