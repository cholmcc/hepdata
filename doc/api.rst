.. _api:

=======================================
Application Programming Interface (API)
=======================================

.. toctree::
   :maxdepth: 2

   pkg_main
   pkg_comb
   pkg_io
   pkg_plot
   pkg_names
   pkg_root

Here is an overview of the packages in this project.  The main package
is :mod:`hepdata` which contains classes for data storage and so on.

The :mod:`hepdata.combiner` package is used when propagating
uncertainties on the results.

Input and output is handled by the :mod:`hepdata.io` package.

Visualisation of results can be done using :mod:`hepdata.plot`.

The :mod:`hepdata.names` package provides utilities for standardised
names and easy set-up. 

.. EOF		
