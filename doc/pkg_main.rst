========================
Main package - `hepdata`
========================

.. toctree::
   :maxdepth: 2

Test cases
----------

See :ref:`test_hepdata`


.. automodapi:: hepdata
	   
.. EOF
   
