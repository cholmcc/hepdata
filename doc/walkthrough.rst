.. _walkthrough:
   
=======================
Walk-through of the API
=======================

.. _create_submission:

Submission meta data
====================

See also :class:`hepdata.Submission`

To start with we need to create a :class:`hepdata.Submission` object.
This is pretty simple via the no-argument constructor or
:class:`hepdata.Submission` or the alias :func:`hepdata.submission`. 

>>> import hepdata as hd
>>> s = hd.Submission()

We can add various pieces of information to the submission, such as

- `Record IDs` : The are identifiers associated with the submission.
  These can be

  - `inSpire`_ record IDs,
  - DOI of the publication,
  - `CDS`_ record IDs,
  - `arXiv`_ identifier,
  - or some other identifier

- `Comment` : A free-form description of the submission

- `Resources` : References to external information, or additional
  files to attach to the submission.

Record identifiers
------------------

To add such an identifier we can do

>>> s.id('inspire',123456)

The method Submission.id returns a reference to the submission object
itself, which means we can chain mutliple calls

>>> s.id('inspire',123456).id('arxiv','nucl-th/123456')

Note, these identifiers wont really be used when submitting the data
to HEPData.  Instead, HEPData will do it's own look-up on `inSpire`_.

A descriptive comment
---------------------

We can also describe the submission in free-form by setting a comment
on the submission. Ususally this is some form of the abstract of the
correspondng publication, but can in principle be any text.  We can
set the comment by

>>> s.comment('Some descriptive text')

.. _autofill:

Automatic fill-in of comment and record IDs
-------------------------------------------

Alternatively we can set all these fields by extracting the data from
inSpire by searching for the publication.

>>> s.fill('Phys.Lett.,B12,1234')

Additional resources
--------------------

Finally, we can add resources to the submission.  These can be

- URL references to more information, for example the Collaboration
  web-page, public notes, and so on.
- Additional files.  These files can then be included in the final
  submission archive and will be attached to the HEPData entry.  These
  could be the original figure image, thumb-nails of the image. 

URL resources are added by

>>> s.resource('https://collaboration.net',
...            'Collaboration web-page',
...            'web-page')  

File resources are added by

>>> s.resource('fig1.pdf','Original figure','application/x-pdf')

Tables of data
==============

Next we should add some tables to the submission.  Exactly which
tables should be added depends a lot on the data you want to submit.
However, bare in mind that a table consist of rows and columns of
data, which implies that we must have equal number of values in each
column (possibly `NaN`).

.. _create_meta:

Table meta information
----------------------

See also :class:`hepdata.Meta`

Let us make a table.  Each table has some meta information (encoded in
:class:`hepdata.Meta`) that we can add.  The required meta information
is

- `name`: The name of the table.  Typically it will be something like
  `Table 1`, but can be any text string you want (but keep it
  short). This name has no particular association with how the data is
  presented in the corresponding paper, although it is often named
  after a figure or table.

- `description`: A description of the data presented in the table.
  This is again free-form text, and we can even mix in LaTeX (rendered
  via MathJax) to make the description more expressive.  Note,
  although this can be longer than the `name` it shouldn't be too
  long.  One can think of it as a caption for the data, and should
  thus give reasonable details.

- `filename`: The file in which we will write the data it self.  All
  meta information of a submission typically goes in one file
  (``submission.yaml`` by convention), while the data of each table
  is stored in separate files.  This field associates the meta data
  with the actual data.

To start a table, we need to define the meta data first

>>> m = s.meta('Table 1', 'Some data', 'tab1.yaml')

Measurement meta information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional information can be attached to the meta data.  This includes

- `keyword`: a mapping from some keyword to some values.  Yes, it is
  valures, plural.  Keywords can in principle be used to filter data
  on `HEPData`_.  In practice, only the following keywords have that
  special meaning

   - `cmenergies`; A list of collision energies in the centre-of-mass
     frame in units of GeV.

   - `reactions`: A list of the reaction(s) studied.  It is written as

       INPUT --> OUTPUT

     where INPUT is often the colliding particles (or nuclei) and
     OUTPUT is typically the kind of particles studied.  Note, that
     both INPUT and OUTPUT should adhere to the names adopted by
     `HEPData`_.
     
   - `observables`: List the measured quantities or corresponding
     theoretical observables.  This can be differential
     cross-sections, nuclear modification factor, and so on. Note,
     that name should adhere to the names adopted by `HEPData`_.
     
   - `phrases`: A list of free-form `short` phrases that are
     associated with the data.  This will typically categorise the
     data, e.g., as depedent on the transverse momentum, from
     inclusive measurements, and so on.

In code, we can add these to the meta data by

>>> m.cmenergies(200)
>>> m.reactions('AU AU --> CHARGED')
>>> m.observables('DN/DYRAP')
>>> m.phrases(['Rapidity Dependence','Heavy-ion`])

The above says that we are looking at Au-Au collision at a centre of
mass energy of :math:`200\,\mathrm{GeV}` and the measurement is the
charged particle rapidity distribution of.  We add the phrases
`Rapidity Dependence` and `Heavy-ion` to make the entry more
searchable.

More meta information and resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We can add more keywords as we like, but `HEPData`_ does not really use
those.

As for the submission it self, we can add additional resources to the
meta information of a table.

>>> m.resource('fig1.eps','Original figure','application/x-eps')
>>> m.resource('thumb-fig1.png','Original figure','image/x-png')

The data table
--------------

See also :class:`hepdata.Table`

Finally, we can add the actual data table to the meta data

>>> t = m.table()

This will create a :class:`hepdata.Table` object and associate it with the meta
data.

Typically the numerical values of the table is stored separately (in
another file) from the meta data.  The filename of the file where the
numerical values are stored is set by the meta data ``data_file``
property (see above).

If the data table is to be in the same file as the meta data, we can
pass ``True`` for the first and only argument to :meth:`Meta.table`.

>>> t = m.table(True)

The table object is where we put the actual data.  To do that, we need
to define some columns of `independent` and `dependent` variables.

`Independent` variables are what we normally put in the abscissa
(`x`-axis), while `dependent` variables are what we put on the
ordinate (`y`-axis).  Examples of independent variables are
:math:`p_{\mathrm{T}}`, :math:`y`, :math:`m_{\mathrm{inv}}` and so on.
Examples of dependent variables are
:math:`E\mathrm{d}^3\sigma/\mathrm{d}p^3`,
:math:`\mathrm{d}N/\mathrm{d}y`,
:math:`1/\sigma\mathrm{d}\sigma/\mathrm{d}m_{\mathrm{inv}}`, and so
on.

.. _create_indep:

Independent variable declaration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

See also :class:`hepdata.Independent`

We declare a indenpendent variable in the code by, for example,

>>> i = t.independent('PT','GEV/C')

to declare :math:`p_{\mathrm{T}}` as the independent variable.  The
method :meth:`Table.independent` takes two arguments: the variable
name and the units (if any) of the variable.  The variable name and
units should, as far as possible, correspond to one of the standard
names and units of `HEPData`_.

The returned object is of the class Independent.  We will see later
how to add values to the column.

We can add as many independent variables to a table as we like, but it
does not make sense to have two or more independent variable columns
with the same variable.

>>> pt = t.independent('PT','GEV/C')
>>> mt = t.independent('MT','GEV/C**2')

If the intent is to have correlation of two independet variables -
e.g., the transverse momentum of pions versus the same for kaons - we
should qualify the variable names

>>> pt_pion = t.independent('PT(PI)','GEV/C')
>>> pt_kaon = t.independent('PT(K)', 'GEV/C')

.. _create_dep:

Dependent variable declaration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

See also :class:`hepdata.Dependent`

Dependent variables are represented by the Dependent class.  We add
such a column to the table by, for example,

>>> d = t.dependent('1/(2*PI*PT)*D2N/DPT/DYRAP','1/(GEV/C)**2')

for the invariant yield measurment.  Dependent variables represents
the measurement and will have uncertainties attach to each value.  We
will see later how to specify the values and uncertainties of the
dependent variables.

Dependent variable qualifiers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unlike the independent variable columns, we can add more information
to the dependent variable columns, known as `qualifiers`.  These can
be specification of particle type(s), specific bins of other variables
(e.g., rapidity, transverse momentum range, centrality, and so on), or
other relevant information. For example, we could specify that the
invariant yeild is measured near midrapidity

>>> d = t.dependent('1/(2*PI*PT)*D2N/DPT/DYRAP','1/(GEV/C)**2')
>>> d.qualifier('YRAP',0)

Qualifiers are, as shown above, declared by the method
:meth:`Dependent.qualifier`, which takes three arguments:

- `name` : The name of the qualifier.  If this corresponds to a
  standard HEPData variable (e.g., :math:`p_{\mathrm{T}}`) then one
  should use that name.
- 'value' : The value of the qualifier.  This can be a single number
  or a string.  So to declare a range, one will encode the range as a
  string, or one can declare two (minimum and maximum) qualifiers.
- `unit` (optional) : The unit of the qualifier.  Again, if the
  qualifier corresponds to a standard HEPData variable or observable
  then one should use the associated units string here.

Some examples of qualifiers

>>> d.qualifier('PT(MIN)', 0.5, 'GEV/C')
>>> d.qualifier('PT(MAX)',  10, 'GEV/C')
>>> d.qualifier('PARTICLE', 'PI+ PI-')
>>> d.qualifier('CENTRALITY', '0-10', 'PCT)

Qualifiers are represented in `HEPData`_ as sub-headers of a column.
Qualifiers can also serve as selection criteria, both within `HEPData`_
and in this package.

.. _fill_in:
   
Fill in values
--------------

Independent variables
~~~~~~~~~~~~~~~~~~~~~

Each value of an independent variable is one of

- a bin, given by a `low` and `high` bound,
- a bin with a non-centred value, characterised by the bound `low` and
  `high` and the `value`
- a single value `value`.

We can add values to an independent variable ``i`` by

>>> i.value(10,20)

for a binned variable, 

>>> i.value(10,20,17)

for a binned value, with a non-centred value, and

>>> i.value(value=17)

for a single value.

Dependent variables
~~~~~~~~~~~~~~~~~~~~~

Dependent variable values are characterised by

- A value
- Some number of uncertainties, which can be a labelled and which are

  - `either` symmetric around the central value, characterised by the
    difference to the central value,
  - `or` asymmetric around the central value, characterised by a
    `minus` and `plus` differences to the central value

We set the central value on the dependent variable ``d`` by

>>> v = d.value(10)

This returns the added :class:`hepdata.Value` object, which we can
uncertainties to.  To add a symmetric uncertainty, we do

>>> v.symerror(math.sqrt(10),'stat')

corresponding to

.. math::
	10\pm\sqrt{10}
	
where we have labelled the uncertainty as statistical (``stat`` is the
conventional label for statistical uncertainties).   To add an
asymmetric uncertainty we do

>>> v.asymerror(2,3,'sys')

where we've labelled the uncertainty with `sys` (``sys`` is the
conventional label for the total systematic uncertainty). That is, we
have the value

.. math::
	10^{+3}_{-2}

We can easily chain the calls since :meth:`hepdata.Value.symerror` and
:meth:`hepdata.Value.asymerror` returns a reference to it self

>>> d.value(10).symerror(3,'stat').asymerror(2,3,'sys')
  
corresponding to

.. math::
	10\pm{3}(\mathrm{stat}){}^{+3}_{-2}(\mathrm{sys})

If we want to give more than one systematic uncertainty we can do so
by adding more uncertainties with appropriate labels.  For example

>>> d.value(123)\
...  .symerror(2,'stat')\
...  .asymerror(1,2,'sys:bg')\
...  .symerror(4,'sys:fit')

corresponding to

.. math::
	123\pm{2}(\mathrm{stat}){}^{+2}_{12}(\mathrm{bg}){}\pm4(\mathrm{fit})

.. _common_uncer:

Common systematic uncertainties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

HEPData_ has no provisions for specifying common uncertainties - that
is, uncertainties that are common (and correlated) to all values in a
column.  However, this package will allow one to specifying these as
`qualifier`s on the column, with the provision that the name of the
qualifier is prefixed by ``sys:``.  For example 

>>> d.qualifier('sys:BACKGROUND',2)
>>> d.qualifier('sys:CENTRALITY',3,'PCT')

If the qualifier has the unit ``PCT`` (or ``%``) then the value is
interpreted as a `relative` value, while other units (or no units) are
interpreted as `absolute` values.   Thus, in the example above, we
have for the value 10

.. math::

   10\pm{2}(\mathrm{background})\pm0.3(\mathrm{centrality})

Method of filling in the values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The hepdata package does not dictate any particular strategy for
filling in the values of the table.  Here, we'll give a few possible
strategies.

Individual values
-----------------
Suppose you have your data in a (NumPy_) arrays

- ``x`` for the independent variable
- ``y`` for the dependent variable
- ``stat`` for statistical uncertainties
- ``sys`` for the systematic uncertainties

and we have the column ``i`` for the independent variable and ``d``
for the (single) dependent variable, we can do

>>> for xx,yy,st,sy in zip(x,y,stat,sys):
...    i.value(value=xx)
...    d.value(yy).symerror(st,'stat').asymerror(sy,'sys')

Alternatively one can use ``map`` or some other functional mapping
function

>>> def add(args):
...     x,y,st,sy = *args
...     i.value(value=x)
...     d.value(y).symerror(st,'stat').asymerror(sy,'sys')
>>> map(add, zip(x,y,stat,sys))

Arrays
------

Suppose we had the NumPy_ arrays as above.  An alternative strategy
will do

>>> i.value(value=x)
>>> d.value(y).symerror(st,'stat').aymerror(sys,'sys')

for binned data, we need to give the lower and upper bounds

>>> i.value(low,high)

What is most efficient really depends on the data.

Finishing off the submission
============================

Once the data has been entered into the data tables and the tables
have been added to the submission, we can finish off the submission.

.. _round_vals:

Rounding to :math:`n` significant digits
----------------------------------------

We do not want to give the results with an excessive number of
significant digits.

For dependent variables, we ideally will give the smallest uncertainty
on each point with 1 (maybe 2) significant digits, and the
remaindering uncertainties and the point value to the same precision.

For independent variables the number of significant digits depend on
the binning, values, or similar.  But we should make sure that we
round the values to reasonable numbers.

We can round all independent variables to ``nx`` significant digits
and all dependent variables to ``ny`` significant digits in a
table ``t`` with meta information ``m`` by one of 

>>> m.roundNsig(nx,ny)
>>> t.roundNsig(nx,ny)

In fact, we can round all numbers in a submission ``s`` by

>>> s.roundNsig(nx,ny)

.. _plot_data:

Visualising the data
--------------------

Sometimes it makes sense to visualise the data to ensure that nothing
went wrong in the conversion.   Suppose we have the meta data ``m``,
we can get the associated data table by

>>> t = m.table()

Once we have the table, we can select columns to visualise.   Suppose
we have a table with the independent variables

.. math::
   p_{\mathrm{T}} \quad m_{\mathrm{T}}

with names ``PT`` and ``MT``, and the dependent variables 

.. math::
   \frac{1}{2\pi p_{\mathrm{T}}\frac{\mathrm{d}^2N}{
     \mathrm{d}y\mathrm{d}p_{\mathrm{T}}}
   \quad
   \frac{1}{2\pi m_{\mathrm{T}}\frac{\mathrm{d}^2N}{
     \mathrm{d}y\mathrm{d}m_{\mathrm{T}}}

with names ``1/(2*PI*PT)*D2N/DYRAP/DPT`` and
``1/(2*PI*MT)*D2N/DYRAP/DMT`` for several rapidities, We want to draw
these as

.. math::
   \frac{1}{2\pi p_{\mathrm{T}}\frac{\mathrm{d}^2N}{
     \mathrm{d}y\mathrm{d}p_{\mathrm{T}}}
   \quad\mathrm{versus}\quad p_{\mathrm{T}}

and

.. math::
   \frac{1}{2\pi m_{\mathrm{T}}\frac{\mathrm{d}^2N}{
     \mathrm{d}y\mathrm{d}m_{\mathrm{T}}}
   \quad\mathrm{versus}\quad m_{\mathrm{T}}

for each rapidity.

We can retrieve the columns pairs as

>>> [pt], d2ndydpt = t.select('PT','1/(2*PI*PT)*D2N/DYRAP/DPT')
>>> [mt], d2ndydmt = t.select('MT','1/(2*PI*MT)*D2N/DYRAP/DMT')

In general, we can use :meth:`hepdata.Table.select` with various kind
of arguments to select columns of the table - for example by name or
by some function.  See that method for more information.

Once we have the columns, we can draw them using
:func:`hepdata.plot.plotXY`

>>> import hepdata.plot as hp
>>> import matplotlib.pyplot as plt
>>>
>>> plt.figure()
>>> for dpt in d2ndydpt:
>>>    hp.plotXY(pt,dpt)
>>> 
>>> plt.figure()
>>> for dmt in d2ndydmt:
>>>    hp.plotXY(mt,dmt)

The function hepdata.plot.plotXY (and underlying class
:class:`hepdata.plot.Plotter`) provides a lot of functionality for
labelling data by qualifiers, customising the plot, and so on.  See
the description of :class:`hepdata.plot.Plotter` for more.

.. _export_sub:

Exporting the final submission
==============================

Once we have ensured the submission is OK, we can export it to file(s)
and possibly to a ZIP archive using the service
:class:`hepdata.io.IO`.


To write all files of submission object ``s``, we can use the service
:func:`hepdata.io.dump` as

>>> import hepdata.io as hi
>>> hi.dump(s)

If we want to dump the files directly to a Zip archive, we can do

>>> hi.dump(s,'submission.zip')

We can check that the data is correct according to the data schema
defined by HEPData_ by passing the keyword ``validator`` as 

>>> hi.dump(s,'submission.zip',validator=True)

both for separate files or archives.

.. _import_data:

Importing data
==============

Unsurprisingly, we can import a submission using
:func:`hepdata.io.load`.   Here's an example of importing a given
submission and then plot all data tables in that submission

.. literalinclude:: ../examples/hepdata_plot.py 

.. _inSpire: http://inspirehep.net
.. _HEPData: http//hepdata.net
.. _CDS: https://cds.cern.ch
.. _arXiv: http://arxiv.org
.. _NumPy: http://numpy.org

.. EOF		
