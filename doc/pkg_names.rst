===============================
Names package - `hepdata.names`
===============================

.. toctree::
   :maxdepth: 2


The classes (except :class:`hepdata.names.Pretty` and
:class:`hepdata.names.Beautifier`) all contains constants that map to
commonly used strings in HEPData.  The definitions are loaded at
import-time from YAML files, and can easily be extended (or redefined)
using auxiliary YAML files and the function :func:`hepdata.names.load`

Extending the HepData names 
============================

The module :mod:`hepdata.names` and associated data (YAML) files
defines

- Phrases that one can associate with a data table 
- Particles to specify on the data table and in reactions 
- Collision systems to specify reactions 
- Units of variables and observables 
- Variables (independent variables) used to label data tables
- Observables (dependent variables) used to label the results. 

The symbols and names defined are by no means exhaustive, and one may
need to define additional names.  This can be done via providing a
YAML file of new names (or redefinitions of existing names). 

The YAML file can contain one or more of the headings 

- ``Phrases`` 
- ``Particles`` 
- ``Systems``
- ``Units``
- ``Variables``
- ``Observables`` 

Under each heading one can have a list of name definitions. This file
can then be read in via

	>>> import hepdata.names as hn
	>>> 
	>>> hn.Helper.load(filename)

The exact format of the list elements depend on the type of the list.

Phrases
-------

Each phrase element consist of 

- ``name`` 
- ``value`` 

both of which are strings.  The ``name`` field specifies the constant
within the class :class:`hepdata.names.Phrase` to define, and must be
a valid Python identifier.  For example

.. code-block:: yaml
		
	Phrases:
	- name: PHRASE_SYMBOL
	  value: The Phrase 

	  
Alternatively one can define a phrase in the declaration of a
`Variable` or `Observable`.  This is done by specifying an object
(Python ``dict``) as an element of the ``phrases`` field.  For example

.. code-block:: yaml
		
	Observables:
	- name: OBSERVABLE_SYMBOL
	  ...
	  phrases: [{PHRASE_SYMBOL: The Phrase}]


However, one should take care to only declare the phrase `once` least
the value is not overwritten by an unexpected value. 

Note, the phrases defined in ``phrases.yaml`` are automatically parsed
out from ``tools/phrases.html`` which in turn is copied from the
`hepdata_submission project
<https://github.com/HEPData/hepdata-submission>`_ by means of the
script ``tools/read_phrs.py``.

Particles
---------

The module :mod:`hepdata.names` defines `most` particles, including
atomic nuclei.  However, one may need to define new particles or
particle combinations.  Again, we must specify a valid Python
identifier for the data member to be added to the class
:class:`hepdata.names.Particle`, as well as a string value for the
HEPData database, and a pretty-print (LaTeX) string.  For example, to
define net-protons, we can do

.. code-block:: yaml
		
	Particles:
	- name: NET_P
	  value: P - PBAR
	  pretty: '\mathrm{p}-\overline{\mathrm{p}}'
	  

Note, the particles defined in ``hepdata/names/data/parts.yaml`` are
automatically parsed out from ``tools/parts.yaml`` which in turn is
copied from the `hepdata_submission project
<https://github.com/HEPData/hepdata-submission>`_ by means of the
script ``tools/read_parts.py``.

Systems 
--------

Collision system can be defined under the header ``Systems``. We must
provide a valid Python identifier for the constant to add to the class
:class:`hepdata.names.System`, and `either` a HEPData string `or`
projectile and target particles (one of the constants defined in
:class:`hepdata.names.Particles`).  We can also give a pretty-print
string (if the HEPData string is given), and whether to consider this
a nucleus-nucleus-like system via the field ``aa``

.. code-block:: yaml
		
	Systems:
	- name: AUAU 
	  value: AU AU
	  pretty: '\mathrm{AU}-\mathrm{AU}'
	  aa: True 
	- name: KP_KM
	  projectile: Particle.K_P
	  target: Particle.K_M
	  

Units
-----

Additional units can be specified in object ``Units``.  Each element
of the list must have a valid Python identifier for the constant to
add to the class :class:`hepdata.names.Unit`, a HEPData value, and a
pretty-print string.  For example

.. code-block:: yaml
		
	Units:
	- name: KELVIN
	  value: K
	  pretty: '\mathrm{K}'
	  

Variables
---------

Variables are the independent quantities in the data tables.  To add a
new variable we give the header `Variables`, containing a list of new
variable declarations.  The declarations must have a valid Python
identifier for the constant to be added to
:class:`hepdata.names.Variable`, a HEPData string value, a pretty
print string and optionally a list of phrases and a unit.  For example

.. code-block:: yaml
		
	Variables: 
	- name: MT_M0
	  value: MT-M
	  pretty: 'm_{\mathrm{T}}-m_{0}'
	  phrases: [Phrase.MT_DEPENDENCE]
	  units: Unit.GEVC2
	- name: MEANPT_RATIO
	  value: MEAN(PT(A)/PT(B))
	  pretty: '\left\langle\frac{p_{\mathrm{T},A}}{p_{\mathrm{T},B}}'
	  phrases: [Phrase.PT_DEPENDENCE]
	  

Note, one can define new phrases in a variable declaration as outlined
above.

Variables are really observables, but here we make a distinction
because an observable as an independent variable may take a different
meaning than if it is in the context of a dependent variable.
However, there's nothing that prevents us from specifying the variable
as an observable too. 

Observables
-----------

Observables are the dependent quantities in the data tables.  To add a
new observable we give the header `Observables`, containing a list of
new observable declarations.  The declarations must have a valid
Python identifier for the constant to be added to
:class:`hepdata.names.Observable`, a HEPData string value, a pretty
print string and optionally a list of phrases and a unit.  For example

.. code-block:: yaml
		
	Observables: 
	- name: MT_M0
	  value: MT-M
          pretty: 'm_{\mathrm{T}}-m_{0}'
          phrases: [Phrase.MT_DEPENDENCE]
          units: Unit.GEVC2	
	- name: TEMP
	  value: TEMP
	  pretty: 'T'
	  phrases: [{TEMPERATURE: Temperature}]
	  units: Unit.MEVC
	- name: NDF
	  value: NDF
	  pretty: '\nu'
	  

Note, one can define new phrases in a observable declaration as
outlined above.

Variables are really observables, but here we make a distinction
because an observable as an independent variable may take a different
meaning than if it is in the context of a dependent variable.
However, there's nothing that prevents us from specifying the variable
as an observable too.

Other ways of extending :mod:`hepdata.names`
--------------------------------------------

One can also extend the :mod:`hepdata.names` database with new names
directly in Python code.  To this end, one can use the class methods

- :meth:`hepdata.names.Phrase.register(name,value)` 
- :meth:`hepdata.names.Particle.register(name,value,pretty)` 
- :meth:`hepdata.names.System.register(name,value,pretty,projectile,target,aa)`
- :meth:`hepdata.names.Unit.register(name,value,pretty)`
- :meth:`hepdata.names.Variable.register(name,value,pretty,units,phrases)`
- :meth:`hepdata.names.Observable.register(name,value,pretty,units,phrases)` 

This approach is best suited for `ad-hoc` definitions - that is
definitions of names that are used once. If a name is used several
times, it is more maintainable and ensures referential integrity to
put the definitions in a YAML file.


Test cases
-----------

See :ref:`test_hepdata.names`

.. automodapi:: hepdata.names
	   
.. EOF
   
