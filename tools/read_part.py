#!/usr/bin/env python3 
"""Script to read in particle names from a YAML file

This can output the list in various formats, including Python code"
"""
import yaml
import re

def make_symbol(nam):
    """Encode a symbol we can attach to a class as an attribute

    Parameters
    ----------
        nam : str 
            The unencoded symbol value 

    Returns
    -------
        sym : str
            The encoded symbol 
    """
    sym = nam\
        .replace('(','_')\
        .replace(')','_')\
        .replace('*','_STAR_')\
        .replace('/','_')
    sym = re.sub(r'\+\+$','_PP', sym)
    sym = re.sub(r'[-][-]$','_MM', sym)
    sym = re.sub(r'\+$','_P', sym)
    sym = re.sub(r'[-]$','_M', sym)
    sym = re.sub('0$','_0', sym)
    sym = sym.replace('-','_')
    sym = '_'.join([s for s in sym.split('_') if s != ''])

    return sym


def make_pretty_nucleus(nam):
    """Pretty print Nucleus name 

    Parameters
    ----------
        nam : str 
            The unencoded symbol value 

    Returns
    -------
        prt : str
            The pretty print version of the name 
    """
    if nam == 'NUCLEUS':
        return 'X'
    if nam == 'BOR':
        return r'\mathrm{B}'
    if nam == 'TUNGSTEN':
        return r'\mathrm{W}'
    if nam == 'DEUT':
        return r'\mathrm{d}'
    if nam == 'NIT':
        return r'\mathrm{N}'
    if nam == 'TRIT':
        return r'\mathrm{H}^{3}'

    return r'\mathrm{'+nam.capitalize()+'}'
    
def make_pretty_gen(nam,cat):
    """General pretty print a name 

    Parameters
    ----------
        nam : str 
            The unencoded symbol value 
        cat : str
            Category of name 

    Returns
    -------
        prt : str
            The pretty print version of the name 
    """
    pp = nam
    fix = {'KS':             r'\mathrm{K}^{0}_{S}',
           'KL':             r'\mathrm{K}^{0}_{L}',
           'UNSPEC':         'x',
           'INVISIBLE':      'x',
           'CHARGED':        r'x^{\pm}', 
           'MULTICHARGED':   r'x^{\pm N}',
           'VLQ':            r'\vec{q}',
           'VLT':            r'\vec{q}^{2/3}',
           'VLB':            r'\vec{q}^{-1/3}',
           'VLX':            r'\vec{q}^{5/3}',
           'VLY':            r'\vec{q}^{-4/3}',
           'VMESON':         r'\vec{\mathrm{Meson}}',
           'CHARGED-MESON':  r'\mathrm{Meson}^{\pm}',
           'SPARTICLE':      r'\tilde{x}',
           'SQUARK':         r'\tilde{q}',
           'GLUINIUM':       r'\tilde{g}',
           'HEAVY-FLAVOR':   r'\mathrm{Heavy flavor}'
    }
    if nam in fix:
        return fix[nam]

    # Put star in exponent
    pp = re.sub(r'\*(BAR|)',r'\1^{*}',pp)
    # Put signs in exponenent 
    pp = re.sub(r'(\*?)(\++|-+|0|1|2)$', r'^{\1\2}',pp)
    # Charmed or Bottomed
    pp = re.sub(r'(\([0-9]+\)|PRIME|)/C([0-9]*)(BAR|)',r'\3_{c\2}\1',pp)
    pp = re.sub(r'(\([0-9]+\)|PRIME|)/B([0-9]*)(BAR|)',r'\3_{b\2}\1',pp)
    # Append a prime 
    pp = re.sub(r'([A-Za-z0-9\\]+|_{[cb][0-9]*})PRIME', r"\1^{\\prime}", pp)
    # Subscript after name
    pp = re.sub(r'([A-Z ]+)([0-9]+)(BAR|)',r'\1\3_{\2}',pp)
    # Special for flavour neutrinos
    pp = re.sub(r'NU(E|MU|TAU)(BAR|)',r'NU\2_{\1}',pp)
    # Special for J/PSI
    pp = re.sub(r'J/PSI',r'\\mathrm{J/\\psi}',pp)
    # Overline over anti-particles 
    pp = re.sub(r'([A-Za-z0-9*\\]+)BAR', r'\\overline{\1}', pp)
    # Put name in math-roman
    pp = re.sub(r'^(\\overline\{|)([A-Z ]+)(\}|)',r'\1\\mathrm{\2}\3',pp)
    # SUSY
    pp = re.sub(r'\\mathrm{([A-Z]+)INO}',r'\\tilde{\\mathrm{\1}}',pp)
    pp = re.sub(r'\\mathrm{S(?!IGMA|TAR|HOWER|Q|QBAR)([A-Z]+)}',
                r'\\tilde{\\mathrm{\1}}',pp)
    # Take some out of math-roman
    pp = re.sub(r'\\mathrm{(LEPTON|QUARK|CHARGED|CHARG|NEUTRAL)}',r'{\1}',pp)
    # Remove double superscripts
    pp = re.sub(r'\^{(\*|\\prime)}\^{(.*)}',r'^{\1\2}',pp)
    spec = {'N':          'n',
            'P':          'p',
            'NUCLEON':    'N',
            'HADRON':     'h',
            'LEPTON':     'l',
            'HIGGS':      'H',
            'AXION':      'A',
            'E':          'e',
            'UQ':         'u',
            'DQ':         'd',
            'SQ':         's',
            'CQ':         'c',
            'TQ':         't',
            'BQ':         'b',
            'UP':         'u',
            'DOWN':       'd',
            'STRANGE':    's',
            'CHARM':      'c',
            'TOP':        't',
            'BOTTOM':     'b',
            'GLUON':      'g',
            'GRAVITON':   'G',
            'NEUTRAL':    'x',
            'CHARGED':    'x',
            'QUARK':      'q',
            'MUON':       r'\\mu',
            'VEE':        r'\\vee',
            'HTRACK':     'H-track',
            'LEPTOQUARK': 'Lepto-quark',
            'VMESON':     'V-Meson',
            'UPSI':       'UPSILON',
            'PION':       r'\\pi',
            'GAUG':       'g',
            'CHARG':      'x',
            'PHOT':       r'\\gamma',
            'AX':         'A',
            'GLU':        'g',
            'GRAVIT':     'G',
            'GOLDST':     'Goldstone'
    }
    cap = { 'JET', 'MESON', 'HEAVY', 'LIGHT', 'FLAVOR', 'LONGLIVED',
            'NARROW', 'KINK', 'STAR', 'SHOWER', 'GREY', 'BLACK' 
    }
    lbs = ['PI',  'GAMMA',   'TAU',   'MU',   'NU',   'RHO',
           'ETA', 'PHI', 'PSI', 'CHI']
    ubs = ['LAMBDA', 'XI', 'LAMBDA', 'SIGMA', 'DELTA', 'UPSILON']

    # Omegas can be mesons and baryons 
    if cat.find('Baryon') > 0:
        ubs.append('OMEGA')
    else:
        lbs.append('OMEGA')
        
    for s in spec:
        pp = re.sub('{'+s+'}','{'+spec[s]+'}',pp)

    for p in cap:
        pp = re.sub(p,p.capitalize(),pp)
        
    for p in lbs:
        pp = re.sub('{'+p+'}',r'{\\'+p.lower()+'}',pp)

    for p in ubs:
        pp = re.sub('{'+p+'}',r'{\\'+p.capitalize()+'}',pp)
        
    return pp

def make_pretty(nam,cat):
    """Pretty print a particle name 
    
    Parameters
    ----------
        nam : str 
            The unencoded symbol value 
        cat : str
            Category of name 

    Returns
    -------
        prt : str
            The pretty print version of the name 
    """    
    if cat == 'Nuclei':
        pp = make_pretty_nucleus(nam)
    else:
        pp = make_pretty_gen(nam,cat)

    return pp

def read(stream):
    """Load particle definitions from a file

    Parameters
    ----------
        stream : File 
            Stream to read from 
    
    Returns
    -------
        parts : list of dictonaries 
            Return the list of read particles.  Each entry is a dict
            with the following fields
    
            - name: The symbol name 
            - value: The value 
            - pretty : The pretty-print version 
    """
    db = yaml.load(stream)
    out = []
    for cat,lst in db['Classes'].items():
        # print('Loading {}'.format(cat))
        for pf in lst:            

            nam = pf['name']
            sym = make_symbol(nam)
            pp  = make_pretty(nam,cat)                

            out.append({'name': sym,
                        'value': nam,
                        'pretty': pp})
            if len(pf['aliases']) <= 0:
                continue
            for a in pf['aliases']:
                if a == '':
                    continue
                out.append({'name': make_symbol(a),
                            'value': a,
                            'pretty': pp})

    return out

def check(out):
    """Check the sanity"""
    for p in out:

        sym = p['name']
        val = p['value']
        prt = p['pretty']
        inv = re.findall(r'[^a-zA-Z0-9_]',sym)
        npr = re.search('[A-Z]{2,}',prt)

        if len(inv) != 0:
            print('Invalid characters in {}: {}'.format(sym,inv),
                  file=sys.stderr)
                  
        if npr is not None:
            print('Whoops, {} -> {}'.format(val,prt),
                  file=sys.stderr)
            
def read_part(input,fmt='plain'):
    """Read particles 

    Parameters 
    ----------
        input : stream 
            Stream to read from 
        fmt : str 
            Output format.  One of 

            - yaml
            - latex 
            - python 
            - plain (or any other value)
    """
    db = read(input)
    check(db)
    
    if fmt == 'latex':
        print(r'\documentclass{article}')
        print(r'\usepackage{longtable}')
        print(r'\begin{document}')
        print(r'\begin{longtable}{lll}')
    elif fmt == 'python':
        print('import hepdata_names as hpn\n\n'
              'def _init_particles():\n'
              '    hpn.Particle',end='')
    elif fmt == 'yaml':
        out = {'Particles': db}
        print(yaml.dump(out))
        return
        
    for e in db:
        sym = e['name']
        val = e['value']
        prt = e['pretty']
            
        if fmt == 'latex':
            print(r'  \verb|'+sym+'|')
            print(r'  & \verb|'+val+'|')
            print(r'  & $'+prt+'$')
            print(r'  \\')
        elif fmt == 'python':
            print("\\\n        .register('"+sym+"',\n"
                  "                  '"+val+"',\n"
                  "                  r'"+prt+"')",end='')
        else:
            print('{:20s} -> {:30s} -> {:40s}'.format(sym,val,prt))
            

    if fmt=='latex':
        print(r'\end{longtable}')
        print(r'\end{document}')
    elif fmt=='python':
        print('\n\n'
              '_init_particles()\n\n'
              '# EOF')
            
    
if __name__ == "__main__":
    import argparse as  ap

    parse = ap.ArgumentParser(description='Script to read in particle '
                              'names from a YAML file')
    oper  = parse.add_mutually_exclusive_group()
    oper.add_argument('--latex',
                      action='store_const',
                      const='latex',
                      dest='fmt',
                      help='Write LaTeX table')
    oper.add_argument('--python',
                      action='store_const',
                      const='python',
                      dest='fmt',
                      help='Write Python code')
    oper.add_argument('--yaml',
                      action='store_const',
                      const='yaml',
                      dest='fmt',
                      help='Dump to screen')
    oper.add_argument('--plain',
                      action='store_const',
                      const='plain',
                      dest='fmt',
                      help='Dump to screen')
    parse.add_argument('input',
                       type=ap.FileType('r'),
                       default='part.yaml',
                       help='Input file name')
    parse.set_defaults(fmt='plain',input='part.yaml')
    a = parse.parse_args()
    
    read_part(a.input,a.fmt)

#
# EOF
#
