#!/usr/bin/env python3 
"""Script to read in phrases from a HTML file

This can output the list in various formats, including Python code"
"""
import html.parser
import re
import sys
import yaml

class Parser(html.parser.HTMLParser):
    def __init__(self):
        super(Parser,self).__init__()
        
        self._table = False
        self._row   = False
        self._cell  = 0
        self._out   = []
        
    def handle_starttag(self,tag,attrs):
        if tag.lower() == 'table':
            self._table = True
            return
        
        if tag.lower() == 'tr' and self._table:
            self._row = True
            self._cell = 0
            return 

        if tag.lower() == 'td' and self._row and self._table:
            self._cell += 1
            return 
        
    def handle_endtag(self,tag):
        if tag.lower() == 'table':
            self._table = False
            return

        if tag.lower() == 'tr' and self._table:
            self._row = False
            return 
        
        if tag.lower() == 'td' and self._row and self._table:
            return 
        
        
    def handle_data(self, data):
        if self._cell != 1 or not self._table or not self._row:
            return

        self.encode(data)

    def encode(self,data):
        ign = ['SUSY']
        if data in ign:
            return
        
        sym = data\
            .upper()\
            .replace(' ','_')\
            .replace('-','_')\
            .replace('/','_')

        rep = {'SUPERSYMMETRY':             'SUSY',
               'CROSS_SECTION':	            'XSEC',
               'DISTRIBUTION':	            'DIST',
               'PROTON_PROTON':	            'PP',
               'PROTON_ANTIPROTON':         'PPBAR',
               'E+_E_':                     'EP_EM',
               'PROTON':                    'P',
               'PION':	                    'PI',
               'KAON':	      	            'K',
               'MUON':	                    'MU',
               'TRANSVERSE_MOMENTUM':       'PT',
               'PSEUDORAPIDITY':            'ETARAP',
               'RAPIDITY':                  'YRAP',
               'ANGULAR':                   'ANG',
               'ASYMMETRY':                 'ASYM',
               'SINGLE_DIFFERENTIAL':       'DIFF',
               'DOUBLE_DIFFERENTIAL':       'DIFF2',
               'TRIPLE_DIFFERENTIAL':       'DIFF3',
               'DIFFERENTIAL':              'DIFF',
               'DEEP_INELASTIC_SCATTERING': 'DIS',
               'SCATTERING':                'SCAT'
        }
        for k,v in rep.items():
            sym = sym.replace(k, v)

        sym = '_'.join([s for s in sym.split('_') if s != ''])


        self._out.append({'name': sym, 'value': data})

    def check(self):
        for p in self._out:
            sym = p['name']
            val = p['value']

            inv = re.findall(r'[^a-zA-Z0-9_]',sym)

            if len(inv) != 0:
                print('Invalid characters in {}: {}'.format(sym,inv),
                      file=sys.stderr)
            

    def format(self,fmt):
        if fmt == 'latex':
            print(r'\documentclass{article}')
            print(r'\usepackage{longtable}')
            print(r'\begin{document}')
            print(r'\begin{longtable}{ll}')
        elif fmt == 'python':
            print('import hepdata_names as hpn\n\n'
                  'def _init_particles():\n'
                  '    hpn.Phrase',end='')
        elif fmt == 'yaml':
            out = {'Phrases': self._out}
            print(yaml.dump(out))
            return

        for p in self._out:
            sym = p['name']
            val = p['value']
        
            if fmt == 'latex':
                print(r'  \verb|'+sym+'|')
                print(r'  & '+val+'')
                print(r'  \\')
            elif fmt == 'python':
                print("\\\n        .register('"+sym+"',\n"
                      "                  '"+val+")",end='')
            else:
                print('{:35s} -> {:s}'.format(sym,val))

        
            

        if fmt=='latex':
            print(r'\end{longtable}')
            print(r'\end{document}')
        elif fmt=='python':
            print('\n\n'
                  '_init_particles()\n\n'
                  '# EOF')
        
    
    @classmethod
    def load(cls,file,fmt='plain'):
        p = Parser()
        p.feed(file.read())
        p.check()
        p.format(fmt)


if __name__ == '__main__':
    import argparse as  ap

    parse = ap.ArgumentParser(description='Script to read in phrase '
                              'names from an HTML file')
    oper  = parse.add_mutually_exclusive_group()
    oper.add_argument('--latex',
                      action='store_const',
                      const='latex',
                      dest='fmt',
                      help='Write LaTeX table')
    oper.add_argument('--python',
                      action='store_const',
                      const='python',
                      dest='fmt',
                      help='Write Python code')
    oper.add_argument('--yaml',
                      action='store_const',
                      const='yaml',
                      dest='fmt',
                      help='Dump to screen')
    oper.add_argument('--plain',
                      action='store_const',
                      const='plain',
                      dest='fmt',
                      help='Dump to screen')
    parse.add_argument('input',
                       type=ap.FileType('r'),
                       default='part.yaml',
                       help='Input file name')
    parse.set_defaults(fmt='plain',input='part.yaml')
    a = parse.parse_args()
    
    Parser.load(a.input,a.fmt)
    
    
