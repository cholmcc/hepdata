"""Package for Input/Output of data in this project

Copyright 2019 Christian Holm Christensen
"""
from . fileio import FileIO
from . zipio import ZipIO
from . tario import TarIO
from . validator import Validator 
from . funcs import load, dump

#
# EOF
#
