"""Code to combine results 

Copyright 2019 Christian Holm Christensen

"""
from hepdata.combiner.observation import Observation
from hepdata.combiner.result import Result
from hepdata.combiner.combiner import Combiner 
from hepdata.combiner.adder2 import Adder2 
from hepdata.combiner.averager import Averager
from hepdata.combiner.linear_sigma import LinearSigma
from hepdata.combiner.linear_variance import LinearVariance

#
# EOF
#

    
        
