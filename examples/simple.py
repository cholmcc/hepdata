"""Make a simple submission

Copyright 2019 Christian Holm Christensen
"""
from hepdata import *

def make(name="simple",same=False,newstyle=False):
    sys   = 'AU AU'
    sqrts = 200
    var   = 'YRAP'
    obs   = ['DN/DYRAP', 'DN/ETARAP']
    parts = ['PI+','PI-', 'K+', 'K-', 'P', 'PBAR']
    phrs  = 'Rapidity Dependence'
    s     = Submission()  #.id('inspire',123456).id('arxiv','bla')    
    t, m  = s.table('d',sys, sqrts, var, obs, parts, phrs,
                    name=name,
                    filename=name+'_tab.yaml' if not newstyle else None,
                    loc='name', both=True,same=same)
    
    yrap   = t.independent('YRAP')
    etarap = t.independent('ETARAP')

    cents  = [(0,10),(10,20),(20,40)]
    dndy   = []
    dndeta = []
    for c in cents:
        dy = t.dependent('DN/DYRAP')\
              .qualifier('CENTRALITY','{}-{}'.format(c[0],c[1]),'PCT')\
              .qualifier('sys:Centrality',(c[1]-c[0])/2,'PCT')\
              .qualifier('sys:Bla',(c[1]-c[0])/2,'PCT')
        de = t.dependent('DN/DETARAP')\
              .qualifier('CENTRALITY','{}-{}'.format(c[0],c[1]),'PCT')\
              .qualifier('sys:Centrality',(c[1]-c[0])/2,'PCT')\
              .qualifier('sys:Bla',(c[1]-c[0])/2,'PCT')
        dndy  .append(dy)
        dndeta.append(de)

    return s,m,t
#
# EOF
# 
