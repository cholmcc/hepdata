#!/usr/bin/env python3

"""Plain plotting of the submission 

Copyright 2019 Christian Holm Christensen 
"""
import sys
sys.path.append('..')

from names import make
from fill import fill
from plot import plot
import hepdata.names as hn

def fancy():
    s, m, t = make()
    fill(t)
    m.roundNsig(2,4)
    b = hn.Beautifier()
    plot(t,b)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    plt.ion()
    fancy()

    import code
    code.interact(local=locals())

    
#
# EOF
#
