#!/usr/bin/env python3

"""Plain plotting of the submission 

Copyright 2019 Christian Holm Christensen 
"""
import sys
sys.path.append('..')

from simple import make
from fill import fill
from plot import plot

def plain():
    s, m, t = make()
    fill(t)
    m.roundNsig(2,4)
    plot(t)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    plt.ion()
    plain()

    import code
    code.interact(local=locals())

    
#
# EOF
#
