"""Make a simple submission

Copyright 2019 Christian Holm Christensen
"""
from hepdata import *
from hepdata.names import *

def make(name="names",same=False):
    
    cents      = [(0,10),(10,20),(20,40)]
    sys        = System.AUAU
    sqrts      = 200
    var        = [Variable.YRAP, Variable.ETARAP]
    obs        = [Observable.Y_DENSITY, Observable.ETA_DENSITY]
    parts      = [Particle.PI_P,Particle.PI_M,
                  Particle.K_P, Particle.K_M,
                  Particle.P,   Particle.PBAR]
    tmp        = [[dict(name=Variable.CENTRALITY,
                        value='{}-{}'.format(c[0],c[1])),
                   dict(name='sys:Centrality',
                        value=(c[1]-c[0])/2, units=Unit.PCT),
                   dict(name='sys:Bla',
                        value=(c[1]-c[0])/2, units=Unit.PCT)]
                  for c in cents]
    quals      = []
    for t in tmp:
        quals.append(t)
        quals.append(t)
    s          = submission()  #.id('inspire',123456).id('arxiv','bla')
    t, m, i, d = columns(s,None,sys,sqrts,var,obs*3,parts,
                         name=name,filename=name+'_tab.yaml',
                         loc='name',more=quals)

    return s,m,t
#
# EOF
# 
