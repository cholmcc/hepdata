#!/usr/bin/env python3 
#
# A simple example of how to read in a ROOT histogram and make a
# submission from it.
#
import sys
sys.path.append('..')
sys.path.append('.')

import os 

from ROOT import TFile, TH1D, TGraphAsymmErrors, TGraphErrors, TMultiGraph

import numpy as np
import matplotlib.pyplot as plt

def makeH():
    """Function to make a histogram"""
    h = TH1D("h","A histogram",100,-4,4);
    h.SetXTitle("X");
    h.SetYTitle("Y");
    h.FillRandom("gaus");
    return h


def makeG(name="g", title="A graph w/symmetric uncertainties"):
    """Function to make a graph with symmetric errors"""
    n  = 10
    x  = np.array([-0.22, 0.05, 0.25, 0.35, 0.5, 0.61,0.7,0.85,0.89,0.95])
    y  = np.array([1,2.9,5.6,7.4,9,9.6,8.7,6.3,4.5,1])
    ex = np.array([.05,.1,.07,.07,.04,.05,.06,.07,.08,.05])
    ey = np.array([.8,.7,.6,.5,.4,.4,.5,.6,.7,.8])
    g  = TGraphErrors(n,x,y,ex,ey)
    g.SetName(name)
    g.SetTitle(title)
    return g

def makeA(name='a',title="A graph w/asymmetric uncertainties"):
    """Function to make a graph with asymmetric errors"""
    s = makeG()
    g = TGraphAsymmErrors()
    for i in range(s.GetN()):
        g.SetPoint(i,s.GetX()[i],s.GetY()[i]);
        g.SetPointError(i,
                        s.GetEX()[i],s.GetEX()[i]-.02,
		        s.GetEY()[i],s.GetEY()[i]-.2);

    g.SetName(name)
    g.SetTitle(title)
    return g
    
def makeM():
    """Function to make a multigraph"""
    m = TMultiGraph("m","A multi-graph");
    m.Add(makeG("m1","stat"));
    m.Add(makeA("m2","sys"));
    return m

def file(mode):
    f = os.path.join(os.path.dirname(__file__),'data.root')
    return TFile.Open(f,mode)

def makeData():
    """Write a ROOT file with data"""
    h = makeH()
    g = makeG()
    a = makeA()
    m = makeM()
    o = file("RECREATE");

    h.Write()
    g.Write()
    a.Write()
    m.Write()

    o.Close()

import hepdata as hd
import hepdata.plot as hp
import hepdata.root as hr 

    
def plot(s,which):
    m = s.meta(which)
    t = m.table()
    [x],[y] = t.select('x','y')

    # We stack uncertainties using propagation 
    calc_kw = dict(calc=hd.Value.stack,stack_calc=hd.Value.linvar)

    fig = plt.figure(which)
    
    hp.plotXY(x,y,
              type='fill',
              error_alpha=.2,
              fmt='s',
              markersize=5,
              calc_kw=calc_kw)
    fig.show()
    
def convert():
    f = file('READ')
    h = f.Get('h')
    a = f.Get('a')
    g = f.Get('g')
    l = f.Get('m')

    s = hd.submission()
    for o in h,a,g,l:
        print(o)
        m = s.meta(o.GetName(),o.GetTitle(),o.GetName()+'.yaml')
        t = m.table()
        x = t.independent('x')
        y = t.dependent('y')
        d = hr.ToHEPData.getData(o)
        hr.ToHEPData.fillIn(x,y, d)
        


    plot(s,'h')
    plot(s,'a')
    plot(s,'g')
    plot(s,'m')

makeData()
convert()

#
# EOF
#
