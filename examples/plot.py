"""Plain plotting of the submission 

Copyright 2019 Christian Holm Christensen 
"""

import hepdata as hd
import hepdata.plot as hp
import matplotlib.pyplot as plt

def plot(t,b=None):
    p = hp.Plotter(b)

    fig, ax = plt.subplots(ncols=2,sharey=True,figsize=(12,10),
                           gridspec_kw={'wspace':0,'hspace':0})

    [yrap],  dndy   = t.select('YRAP',  'DN/DYRAP')
    [etarap],dndeta = t.select('ETARAP','DN/DETARAP')

    # We stack uncertainties using propagation 
    calc_kw = dict(calc=hd.Value.stack,stack_calc=hd.Value.linvar)
    
    for dy in dndy:
        p.plotXY(yrap,dy,
                 axes=ax[0],
                 type='fill',
                 error_alpha=.2,
                 qualifiers='*',
                 fmt='s',
                 markersize=5,
                 calc_kw=calc_kw,
                 common_kw={'type':'bar'})
    
    for deta in dndeta:
        params = p.emptyParams(deta, calc_kw=calc_kw)
        params['stat']['type'] = 'bar'
        params['more'].update(dict(type='line',linestyle='--'))
        # A more traditional form:
        #params['more'].update(dict(type='bar',linewidth=0,capsize=5))
        params['sys'].update(dict(type='marker',markersize=5,alpha=.5))

        cparams = p.emptyCommonParams(deta)
        cparams['Centrality'].update(dict(type='fill',label='Whatever'))
        cparams['Bla'].update(dict(type='bar',linewidth=3))
        
        p.plotXY(etarap,deta,
                 axes=ax[1],
                 fmt='o',
                 markerfacecolor='white',
                 calc_kw=calc_kw,
                 labelled=True,
                 common_kw=dict(yloc='middle',type='fill',
                                params=list(cparams.values())),
                 qualifiers=['NAME','CENTRALITY'],
                 params=list(params.values()))

    ax[0].legend()
    ax[1].legend()
    plt.tight_layout()

#
# EOF
#
