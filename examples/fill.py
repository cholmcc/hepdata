"""A simple function to fill in values 

Copyright 2019 Christian Holm Christensen
"""
import math
import random

def nm(y,a,sigma):
    return a/(math.sqrt(2*math.pi)*sigma)*math.exp(-0.5*y**2/sigma**2)
        
def fy(a,y):
    return nm(y,a,1)

def y2eta(y,a):
    t = math.tanh(y)
    t = -((1+2*a**2)*t**2
          - 2*t*math.sqrt(a**2+1)*math.sqrt(a**2*t**2+1) + 1)/(t**2-1)
    return 0.5 * math.log(t)
    
def feta(fy,eta,a):
    return fy / math.sqrt(1 + a**2 / math.cosh(eta))

def se(v,f=.05):
    return math.fabs(random.gauss(f*v,f/2*v))

def ae(v,f=0.1):
    return se(v,f), se(v,f)

def fill(t):
    random.seed(123456)

    [yrap],  dndyrap   = t.select('YRAP',  'DN/DYRAP')
    [etarap],dndetarap = t.select('ETARAP','DN/DETARAP')
    tmp   = [float(c) 
             for dndy in dndyrap
             for q in dndy.qualifiers('CENTRALITY')
             for c in q['value'].split('-')]
    cents = [(l,h) for l,h in zip(tmp[::2],tmp[1::2])]
    
    ymin = -3
    ymax = +3
    dy   = .2
    y    = ymin
    ptm  = 2.5
    a    = [1000, 800, 400]
    iy   = [0] * len(cents)
    ieta = [0] * len(cents)
    while y < ymax:
        xy   = y + dy/2
        xeta = y2eta(xy,ptm)
        deta = y2eta(y,ptm) - y2eta(y+dy,ptm)
        yrap  .value(y,y+dy)
        etarap.value(value=xeta)

        for ic,(aa,dndy,dndeta) in enumerate(zip(a, dndyrap, dndetarap)):
            yy = fy(aa,y+dy/2)
            dndy.value(yy)\
                .symerror(se(yy), label='stat').asymerror(*ae(yy),label='sys')
            iy[ic] += dy * yy
            
            yeta = feta(yy,xeta,ptm)
            dndeta.value(yeta)\
                  .symerror(se(yeta),label='stat')\
                  .asymerror(*ae(yeta),label='sys')\
                  .symerror(se(yeta),label='more')
            ieta[ic] += deta * yeta
            
        y += dy

#
# EOF
#

