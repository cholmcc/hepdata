# A library for HepData submissions 

This set module of Python code aims to make it easy to prepare a
submission for [HEPData](http://hepdata.net).  The interface is
distinctly Object-Oriented, and certain modern
[Pythonic](https://stackoverflow.com/questions/25011078/what-does-pythonic-mean)
principles are not employed.  You are of course welcome to submit
suggestions for more improvements. 


## Documentation 

The package is documented in [Gitlab Pages](https://cholmcc.gitlab.io/hepdata)


