#
#
#
PYTHON		= python3
UNITTEST_ARGS	=
ifndef PYPI_URL
PYPI            :=
else
PYPI            := --repository-url $(PYPI_URL)
endif

test:
	$(PYTHON) -m unittest discover -s test $(UNITTEST_ARGS)

doc:
	$(MAKE) -C doc

clean:
	$(MAKE) -C doc clean
	$(shell find . -name "*~" | xargs rm -f)
	$(shell find . -name "__pycache__" | xargs rm -rf)
	rm -f test/*.yaml test/*.old test/*.zip test/*.tgz
	rm -f *.zip *.tgz *.yaml *.old
	rm -rf build dist *hepdata*.egg-info public .eggs
	rm -f examples/data.root


devel-install:
	pip install --break-system-packages --editable .

docker-run:
	docker run -ti --rm -v $(PWD):/home python:3 \
		/bin/bash -c "cd /home && make docker-build"

docker-build:
	apt-get update -y
	apt-get install -y graphviz
	pip install -U sphinx sphinx_autorun sphinx_automodapi \
		matplotlib pyyaml jsonschema hepdata-validator 
	$(MAKE) -C doc
	rm -rf public 
	cp -a doc/.build public


pipdist:
	$(PYTHON) setup.py sdist bdist_wheel 

pypi:
	$(PYTHON) -m twine upload $(PYPI) dist/pyhepdata*

.PHONY:	test doc

